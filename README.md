# Kubernetes 101 

It is a way to manage and deploy containers at scale! Works like terraform but for containers, with some extra features. 

Includes:

- Self healing
- Monioroing
- Loved by the community

### How does it work?

K8 is setup as cluster (like ansible).

- k8 Control panel
- k8 worker nodes

Inside the nodes you have:

- Namespaces (like a vpc)
- Services (endpoinds for pods, like LB)
- Pods (container + launch templates)

### Pre requisits And pre install notes

There are different ways to get a k8 cluster. It's generally a bit tricky to set up by yourself. The following are options

https://kubernetes.io/docs/setup/production-environment/tools/

- Use kubespray (ansible)
- kubeadm

Other serives of kubernetes that are managed:

- aws EKS
- Google GKE

Different distro of K8:

- k8 offical
- minikube
- MicroK8s


### our setup 

We'll use kubespray:

- https://github.com/kubernetes-sigs/kubespray

We're going to need:

- 1 k8 control panel - t3a.xlarge
- 2 k8 woker nodes  - t3a.xlarge
- 1 ansible machine with the right access t3a.xlarge


1. Creat all your instances
2. ssh into the ansible machine
3. git clone the kubespray
4. intall the requierments
5. copy the inventory and add the need private ips of the Control panel + nodes
6. Allow networking between the machines
7. generate a new key pair for the ansible to conect to the Control panel + nodes
8. Add they public key to autorized_keys of Control panel + nodes



## Setup Steps

### 1) Start Machines

We need 4 machines t3a.xlarge. Starte these and give them apropriate tags:

- Ansible
- k8 control panel
- k8 Agent A
- k8 Agent B

### 2) Allow SG networking

All machines:

- Allow cohort_9_home_ips

Ansible will have to connect to k8 machines to setup.

- Allow ansible into these machines - we can just add a group that allows cohort9_home_ips

### 3) Create and Share Key Pairs

In the Ansible Machine:

- Create a new key pair

```bash

$ ssh-keygen 
> Add a /path/name 
> no password

```

Loggin to our k8 machines:

- Add/append the public key pair to the ~/.ssh/authorized_keys file

**Test** step 3 and 4 by trying to loggin to each k8 machine via the ansible machine using the new keypair and the **PRIVATE IP** - note only the private IP will be allowed in.

**note** if you don't manually loggin the first time, ansible will requiere the option of HostKeyStricklyChecking to be off. Or it will hang. 

### 4) Clone Kubesprays to ansible machine

**Note** We migh need these dependencies:

```bash

# python3 
# git

```

Git clone the kubespray repo

```bash

$ git clone https://github.com/kubernetes-sigs/kubespray.git

```

Install dependencies: 

```bash

sudo apt update

sudo apt install python3-pip -y

```


Run the requierments.txt 

```bash

# Install dependencies from ``requirements.txt``
sudo pip3 install -r requirements.txt

```

### 5) Edit Inventory File in Kubespray & other setup

Kubespray has a sample inventory file we can copy and then edit.

```bash
# Copy ``inventory/sample`` as ``inventory/mycluster``
cp -rfp inventory/sample inventory/mycluster
```

On the inventory file add you k8s private IPs and specify the ssh. SSH key can also be specified when calling the ansible playbook.

In the `kubesprat/inventory/mycluster/inventory.ini` uncomment the 3 first nodes and add the private ip to the `ansible_host` and to the `ip`:

```YAML
[all]
node1 ansible_host=172.31.31.109  ip=172.31.31.109 etcd_member_name=etcd1
node2 ansible_host=172.31.23.177  ip=172.31.23.177 etcd_member_name=etcd2
node3 ansible_host=172.31.17.117  ip=172.31.17.117 etcd_member_name=etcd3

```

Also specify the key and user for each node:

```YAML
[all]
node1 ansible_host=172.31.31.109  ip=172.31.31.109 etcd_member_name=etcd1 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu
node2 ansible_host=172.31.23.177  ip=172.31.23.177 etcd_member_name=etcd2 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu
node3 ansible_host=172.31.17.117  ip=172.31.17.117 etcd_member_name=etcd3 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu
```

Then uncomment the correct sections for `node1` to be the control panel and `node2` and `node3` to be `kube_nodes`. Also uncomment `etcd` which is k8's monitoring:

```YAML
[kube_control_plane]
node1
# node2
# node3

[etcd]
node1
node2
node3

[kube_node]
node2
node3
```

Then do this

```bash

# Update Ansible inventory file with inventory builder
# run this in the root folder of kubespray
declare -a IPS=(172.31.31.109 172.31.23.177 172.31.17.117)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

# Review and change parameters under ``inventory/mycluster/group_vars``
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml

```

### 6) Run Playbook with inventory File

```bash

sudo apt install ansible -y

ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b 

# add this option if you want to specify key here --private-key=~/.ssh/k8_key.pem
```

## Kubernetes and Kubectl!

When doing these task I want you to start your own kubernetes cheatsheet! So keep the commands! 

**Task 1** NAMESAPCES

What are name spaces?

- Check existing NAMESPACES
- Make a new NS
- Make another!
- Delete a NS. How cool!


**Task 2** DEPLOYMENT FILES - Now we need to deploy some pods (these have containers)

- What is a deployment file?
- what is the language?
- What "makes it a deployment file"
- What are the 3 sections?
- Why are labels important?
- Where do you specify the container IMAGE to use?

- How do you run a deploy file into your deployment file into your new Namesapce?
- How do your check how many pods are running?
- How do you kill a pod?
- how do you describe a pod? 
- What extra information do you get? 
- How do you ssh into a pod? 

- How do you modify a deployment setup? 
- change the Image?
- Change the numbe of pods? 

**Task 3** Service Files

- create a cluster service file
- it needs to target the webapp you just created
- Now how do you check your services running?
- Curl the serive
- What other main services exist? 


**Task 4** document all your commands

### k8 Control Panel

The k8 control panel is where we can define services, deployments and generally describe the disred stated of these in the k8 worker nodes. 

General terminology around k8 control pannel:

- `kubectl`
- pods
- K8 nodes
- namepsaces
- deployment files
- Services 

In your k8 Control Panel you interact with your kluster mainly via ´kubectl´ - The kubernetes CLI to make things happen. 

#### Starting the serive

Once you start your cluster you'll need to run:

``` bash
# run to start the kubectl service correctly
$ sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
```


#### Namespaces

Is a section area, where you can then deploy services and pods into. Like a VPC, but inside the Nodes.

```bash

# gets all available ns in cluster
$ kubeclt get ns

# create a NS
$ kubectl create ns <name>

# delete a NS
$ kubectl delete ns <namespace>
```

#### Deployment & Serives

Two main sections that you will work on. They are a declaritve way to deploy pods into the cluster and create neworking "services" to connect and expose these. 

These are written in YAML.

**deployment file**

Deployment files have a few sections. 

```bash
$ kubectl apply -f deployment.yaml -n webapp

$ kubectl get pods -n webapp

$ kubectl describe pods -n webapp
```

This is great! GREAT STUFF... but where is my nignx? 

To see it we need a service.

**Services in k8**

**Cluster** Serivice - This exposes a group of pods through one internal ip. Does an internal LB for the pods.

```bash
$ kubectl apply -f service_nginx.yaml -n webapp 

$ kubectl get services -n webapp
```

**NodePort**

Nodeport expose an ip to a resource inside one node.

Does not make much sense.


**Loadbalancer** 

**ExternalName**

#### Ingress controler

Ingress controller allows the outside world to connect to specific services or pods via ingress rules. 

Example:

- If you are comming via the dns petfunerary.academyal.com ---> you endup in the correct website / pods / service
- If you are comming via petclinic.academyal.com --> you endup in the correct website / pods / service with the correct petclinic website

We'll install HA proxy Ingress controler:

https://github.com/haproxytech/kubernetes-ingress 

**Step 1** in your Control panel

```bash
$ git clone https://github.com/haproxytech/kubernetes-ingress.git
```

**Step 2** Apply the deployment of the ingress controller

```bash
$ kubectl apply -f deploy/haproxy-ingress.yaml
```